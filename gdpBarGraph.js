
// define margin - margin convention http://bl.ocks.org/mbostock/3019563
let margin = { top: 20, right: 10, bottom: 100, left: 40 },
    width = 700 - margin.right - margin.left,
    height = 500 - margin.top - margin.bottom;

// define svg - https://developer.mozilla.org/en-US/docs/Web/SVG/Element/org
let svg = d3.select('body')
    .append('svg')
    .attr({
        "width": width + margin.right + margin.left,
        "height": height + margin.top + margin.bottom
    })
    .append('g')
    .attr("transform", `translate(${margin.left}, ${margin.right})`);

// see D3 API reference for info on AXIS and SCALES
// https://github.com/d3/d3-3.x-api-reference/blob/master/SVG-Axes.md
const xScale = d3.scale.ordinal()
    .rangeRoundBands([0, width], 0.2, 0.2);

const yScale = d3.scale.linear()
    .range([height, 0]);

const xAxis = d3.svg.axis()
    .scale(xScale)
    .orient("bottom");

const yAxis = d3.svg.axis()
    .scale(yScale)
    .orient("left");

// load data
d3.csv("gdp.csv", (error, data) => {
    if (error) {
        console.log("Error: data not loaded");
    }

    data.forEach(d => {
        d.gdp = +d.gdp; // convert from string to number
        d.country = d.country;
        console.log(d.gdp)
    });

    data.sort((a,b) => { return b.gdp - a.gdp; })

    // specify the domain of x and y scales.
    xScale.domain(data.map(d=>d.country));
    yScale.domain([0, d3.max(data, d=>d.gdp)]);

    // draw the bars
    svg.selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr("height", 0)
        .attr("y", height)
        .transition().duration(3000)
        .delay((d,i)=>i*200)
        .attr({
            'x': d => xScale(d.country),
            'y': d => yScale(d.gdp),
            'width': xScale.rangeBand(),
            'height': d => height - yScale(d.gdp)
        })
        .style("fill", (d,i)=>`rgb(20,20,${(i*30)+100})`);

    // label the bars
    svg.selectAll('text')
        .data(data)
        .enter()
        .append('text')
        .text(d => d.gdp )
        .attr('x', d=> xScale(d.country) + xScale.rangeBand()/2)
        .attr('y', d=> yScale(d.gdp) + 12)
        .style("fill", "white")
        .style("text-anchor", "middle")
        .style("font-size", "12px")

    // draw the xAxis
    svg.append('g')
        .attr("class", "x axis")
        .attr("transform", `translate(0, ${height})`)
        .call(xAxis)
        .selectAll('text')
        .attr("transform", "rotate(-60)")
        .attr("dx", "-.8em")
        .attr("dy", ".25em")
        .style("text-anchor", "end")
        .style("font-size", "12px")

    svg.append('g')
        .attr("class", "y axis")
        .call(yAxis);
});
